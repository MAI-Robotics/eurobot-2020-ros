import rospy

from simple_pid import PID
from std_msgs import Int32, Int16, Float32
class PIDController():

    def __init__(self):
        rospy.init_node("pid_controller")
        self.pid = PID(output_limits=(-255,255))
        self.pid.Kd = rospy.get_param('~Kp', 10)
        self.pid.Ki = rospy.get_param('~Ki', 10)
        self.pid.Kp = rospy.get_param('~Kp', 0.001)
        self.rate = rospy.get_param('~rate', 50)
        self.timeout = rospy.get_param('~timeout', 0.2)
        self.min_vel = rospy.get_param('~minVel', 0.1)

        self.lastEncoderTicks = 0
        self.lastEncoderTime = rospy.Time.now()
        self.encoderTicksPerSecond = 0

        self.motor_cmd_pub = rospy.Publisher("motor_cmd", Int16, queue_size=1)
        rospy.Subscriber("wheel", Int32, self.wheelCallback) 
        rospy.Subscriber("wheel_vtarget", Float32, self.targetCallback)

    def targetCallback(self, msg):
        if msg.data < self.min_vel:
            self.motor_cmd_pub.publish(0)
            self.pid.setpoint = 0
        else:
            self.pid.setpoint = msg.data
    
    def wheelCallback(self, msg):
        currentEncoderTicks = msg.data
        if (currentEncoderTicks == self.lastEncoderTicks):
            return

        encoderTime = rospy.Time.now()

        delta = self.lastEncoderTime - encoderTime
        delta = delta.to_sec()

        deltaTicks = self.lastEncoderTicks - currentEncoderTicks

        self.encoderTicksPerSecond = deltaTicks / delta
        self.lastEncoderTicks = currentEncoderTicks
        self.lastEncoderTime = encoderTime

    def spin(self):
    #####################################################
        self.r = rospy.Rate(self.rate)

        while not rospy.is_shutdown():
            self.spinOnce()
            
    #####################################################
    def spinOnce(self):
    #####################################################
        while not rospy.is_shutdown() and self.lastEncoderTime - rospy.Time.now() > self.timeout:
            self.motor_cmd_pub.publish(self.pid(self.encoderTicksPerSecond))
            self.r.sleep()

if __name__ == '__main__':
    """ main """
    try:
        pidController = PIDController()
        pidController.spin()
    except rospy.ROSInterruptException:
        pass