var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr); // N.B ideally should return xhr.response
      } else {
        callback(status, xhr);
      }
    };
    xhr.send();
};

var date_object = new Date();

var shutdown_btn        = document.getElementById("shutdown_btn");
var reset_task_btn      = document.getElementById("reset_task_btn");
var kill_task_btn       = document.getElementById("kill_task_btn");
var set_team_blue_btn   = document.getElementById("set_team_blue_btn");
var set_team_yellow_btn = document.getElementById("set_team_yellow_btn");

var gripper_open_action_btn  = document.getElementById("gripper_open_action_btn");
var gripper_close_action_btn = document.getElementById("gripper_close_action_btn");

var gripper0_toggle_action_btn = document.getElementById("gripper0_toggle_action_btn");
var gripper1_toggle_action_btn = document.getElementById("gripper1_toggle_action_btn");
var gripper2_toggle_action_btn = document.getElementById("gripper2_toggle_action_btn");
var gripper3_toggle_action_btn = document.getElementById("gripper3_toggle_action_btn");
var gripper4_toggle_action_btn = document.getElementById("gripper4_toggle_action_btn");

var small_bot_activation_action_btn  = document.getElementById("small_bot_activation_action_btn");

var robot_x   = document.getElementById("robot_x");
var robot_y   = document.getElementById("robot_y");
var robot_yaw = document.getElementById("robot_yaw");

var reset_odometry_btn = document.getElementById("reset_odometry_btn");

function update_robot_position()
{
    getJSON(window.location.origin + "/get_robot_position", (status, xhr)=>{
        var data = xhr.response;
        robot_x.value   = (data.x).toFixed(5);
        robot_y.value   = (data.y).toFixed(5);
        robot_yaw.value = ((data.yaw / Math.PI) * 180.0).toFixed(5);
    });
}

setInterval(update_robot_position, 1000);

reset_odometry_btn.onclick = function()
{
    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/reset_odometry" + q, ()=>{});
}

small_bot_activation_action_btn.onclick = function () {
    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/activate_lil_bot" + q, ()=>{});
}

gripper_open_action_btn.onclick = function()
{
    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/open_gripper" + q, ()=>{});
}

gripper_close_action_btn.onclick = function()
{
    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/close_gripper" + q, ()=>{});
}

gripper0_toggle_action_btn.onclick = function()
{
    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/toggle_gripper0" + q, ()=>{});
}

gripper1_toggle_action_btn.onclick = function()
{
    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/toggle_gripper1" + q, ()=>{});
}

gripper2_toggle_action_btn.onclick = function()
{
    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/toggle_gripper2" + q, ()=>{});
}

gripper3_toggle_action_btn.onclick = function()
{
    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/toggle_gripper3" + q, ()=>{});
}

gripper4_toggle_action_btn.onclick = function()
{
    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/toggle_gripper4" + q, ()=>{});
}

reset_task_btn.onclick = function()
{
    console.log("reset task");

    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/eurobot_task_reset" + q, ()=>{});
}

set_team_blue_btn.onclick = function()
{
    console.log("Set team to yellow");
    getJSON(window.location.origin + "/set_blue", ()=>{});
}

set_team_yellow_btn.onclick = function()
{
    console.log("Set team to yellow");

    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/set_yellow" + q, ()=>{});
}

eurobot_kill_task_btn.onclick = function()
{
    console.log("kill task");

    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/eurobot_kill_task" + q, ()=>{});
}

shutdown_btn.onclick = function()
{
    console.log("shutdown")

    var q = "?t=" + date_object.getTime();
    getJSON(window.location.origin + "/shutdown" + q, ()=>{});
}